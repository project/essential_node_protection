This module protects essential nodes from deletion.

Nodes like the front page, 404 not found page and the 403 access denied page are often configured in the basic site configuration. Therefore it is not desirable that these would be removed by a content manager for example.
