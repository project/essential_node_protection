<?php

namespace Drupal\essential_node_protection\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Essential Node Protection settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  protected $pathValidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pathValidator = $container->get('path.validator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'essential_node_protection_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['essential_node_protection.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('essential_node_protection.settings');

    $form['#tree'] = TRUE;

    $form['site_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Protected nodes'),
      '#description' => $this->t('Select the pages that should be protected from deletion. It will not be possible to delete these nodes.'),
    ];

    $form['site_settings']['front'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Protect front page'),
      '#default_value' => $config->get('site_settings.front'),
    ];

    $form['site_settings']['403'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Protect 403 page'),
      '#default_value' => $config->get('site_settings.403'),
    ];

    $form['site_settings']['404'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Protect 404 page'),
      '#default_value' => $config->get('site_settings.404'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('essential_node_protection.settings');
    $values = $form_state->getValue('site_settings');
    $changes = array_diff_assoc($config->get('site_settings'), $values);
    $config->set('site_settings', $values);
    $config->save();

    if (!empty($changes)) {
      $this->invalidateCacheForChanges($changes);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Invalidates the cache of the nodes whose settings have been changed.
   *
   * @param array $changes
   *   Array containing the changes in the form.
   */
  private function invalidateCacheForChanges(array $changes) {
    $site_config = $this->config('system.site')->get('page');
    $tags_to_invalidate = [];

    foreach (array_keys($changes) as $changed_type) {
      $node_path = $site_config[$changed_type] ?? NULL;

      if ($node_path) {
        $node_url = $this->pathValidator->getUrlIfValid($node_path);
        $nid = $node_url->getRouteParameters()['node'] ?? NULL;

        if ($nid) {
          $tags_to_invalidate[] = 'node:' . $nid;
        }
      }
    }

    if (!empty($tags_to_invalidate)) {
      Cache::invalidateTags($tags_to_invalidate);
    }
  }

}
