<?php

namespace Drupal\essential_node_protection;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeAccessControlHandler as OriginalNodeAccessControlHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the access control handler for the node entity type.
 */
class NodeAccessControlHandler extends OriginalNodeAccessControlHandler {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $pathAliasManager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance(
      $container,
      $entity_type
    );
    $instance->configFactory = $container->get('config.factory');
    $instance->pathAliasManager = $container->get('path_alias.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($operation === 'delete' && $this->isEssential($entity)) {
      $result = AccessResult::forbidden('Node is essential and therefore cannot be deleted.');
      return $return_as_object ? $result : $result->isAllowed();
    }

    return parent::access($entity, $operation, $account, $return_as_object);
  }

  /**
   * Returns true if entity is set as frontpage, 404/403 page in site settings.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   True if the entity is essential.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function isEssential(EntityInterface $entity): bool {
    $module_config = $this->getModuleConfig()->get('site_settings');
    $site_config = $this->configFactory->get('system.site')->get('page');

    $path_alias = $entity->toUrl()->toString();
    $system_path = $this->pathAliasManager->getPathByAlias($path_alias);

    $match = array_search($system_path, $site_config);

    // If there is a match and the protection for that match is enabled.
    return $match && $module_config[$match] === 1;
  }

  /**
   * Get the settings for this module.
   */
  private function getModuleConfig(): ImmutableConfig|Config {
    return $this->configFactory->get('essential_node_protection.settings');
  }

}
